package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type Service struct {
	database *sql.DB
}

type User struct {
	Name      string
	Address   string
	Age       int
	CreatedAt time.Time
}

func initDB() (*sql.DB, error) {
	db, err := sql.Open("mysql", "user:password@/dbname")
	if err != nil {
		return db, err
	}

	return db, nil
}

func main() {
	db, err := initDB()
	if err != nil {
		log.Fatal(err)
	}

	svc := &Service{
		database: db,
	}

	fmt.Println(svc)
}

func (s *Service) SaveUser(u User) error {
	_, err := s.database.Exec(`INSERT INTO users (name, address, age, created_at) VALUES (?, ?, ?, ?)`,
		u.Name,
		u.Address,
		u.Age,
		u.CreatedAt)
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) GetUserByName(name string) ([]User, error) {
	var users []User

	res, err := s.database.Query(`SELECT name, address, age, created_at FROM users WHERE name = ?`, name)

	if err != nil {
		return users, err
	}

	for res.Next() {
		var u User
		err := res.Scan(&u.Name, &u.Address, &u.Age, &u.CreatedAt)
		if err != nil {
			return users, err
		}

		users = append(users, u)
	}

	return users, err
}
