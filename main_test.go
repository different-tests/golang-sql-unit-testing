package main

import (
	"database/sql"
	"errors"
	"log"
	"reflect"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
)

func Test_SaveUser(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	if err != nil {
		log.Fatal("error init mock", err)
	}
	defer db.Close()
	svc := Service{database: db}
	// first test case, no error
	userSuccess := User{
		Name:      "John",
		Address:   "USA",
		Age:       35,
		CreatedAt: time.Now(),
	}
	mock.ExpectExec(`INSERT INTO users (name, address, age, created_at) VALUES (?, ?, ?, ?)`).
		WithArgs(userSuccess.Name, userSuccess.Address, userSuccess.Age, userSuccess.CreatedAt).
		WillReturnResult(sqlmock.NewResult(1, 1))
	err = svc.SaveUser(userSuccess)
	if err != nil {
		t.Errorf("[Test_SaveUser] user success assertion failed, got err %v", err)
	}
	userFailed := User{
		Name:      "Sally",
		Address:   "USA",
		Age:       35,
		CreatedAt: time.Now(),
	}
	mock.ExpectExec(`INSERT INTO users (name, address, age, created_at) VALUES (?, ?, ?, ?)`).
		WithArgs(userFailed.Name, userFailed.Address, userFailed.Age, userFailed.CreatedAt).
		WillReturnError(sql.ErrConnDone)
	err = svc.SaveUser(userFailed)
	if !errors.Is(err, sql.ErrConnDone) {
		t.Errorf("[Test_SaveUser] user error assertion failed, got err %v", err)
	}

}

func Test_GetUserByName(t *testing.T) {

	mockedTime := time.Now()

	tests := []struct {
		name          string
		inputUser     User
		expectedUsers []User
		expectedErr   error
	}{

		{

			name: "error on query",
			inputUser: User{
				Name: "Buck",
			},

			expectedErr:   sql.ErrConnDone,
			expectedUsers: nil,
		},
		{
			name: "success on all query",
			inputUser: User{
				Name: "Angel",
			},
			expectedErr: nil,
			expectedUsers: []User{
				{
					Name:      "Angel",
					Address:   "USA",
					Age:       24,
					CreatedAt: mockedTime,
				},
				{
					Name:      "Angel",
					Address:   "UK",
					Age:       38,
					CreatedAt: mockedTime,
				},
			},
		},
	}

	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	if err != nil {
		log.Fatal("error init mock", err)
	}
	defer db.Close()
	// mock for 1st TC (error on query)
	mock.ExpectQuery(`SELECT name, address, age, created_at FROM users WHERE name = ?`).
		WithArgs("Angel").
		WillReturnRows(sqlmock.NewRows([]string{"name", "address", "age", "created_at"}).AddRow("Angel","USA",24,mockedTime).AddRow("Angel", "UK", 38, mockedTime))

	for _, tc := range tests {
		svc := Service{
			database: db,
		}

		uResult, err := svc.GetUserByName(tc.inputUser.Name)
		if !reflect.DeepEqual(uResult, tc.expectedUsers) {
			t.Errorf("[Test GetUserByName] assertion user failed at tc %v, got %+v, expect %+v", tc.name, uResult, tc.expectedUsers)
			if err != tc.expectedErr {
				t.Errorf("[Test GetUserByName] assertion err failed at tc %v, got %+v, expect %+v", tc.name, err, tc.expectedErr)
			}
		}
	}
}
